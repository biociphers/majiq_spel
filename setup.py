from setuptools import setup

setup(
    name='majiq_spel',
    packages=['majiq_spel'],
    version='0.3',
    description='',
    include_package_data=True,
    install_requires=['colour==0.1.5', 'Jinja2==3.1.2', ],
    entry_points={'console_scripts': ['majiq_spel = majiq_spel.run:main']},
    package_data={'majiq_spel': [
        'templates/*.html',
        'templates/*.xml',
        'static/majiq_spel/css/*.css',
        'static/majiq_spel/img/*.png',
        'static/majiq_spel/js/*.js',
    ]}
)
