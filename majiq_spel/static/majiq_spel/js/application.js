/**
 * Translate lsv bins in to a state that can be graphed.
 * @param lsv_bins - lsv bins data from voila
 * @returns {Array}
 */
function translate_lsv_bins(lsv_bins) {
    var num_samples = 20;
    var tmp_bins = [];
    var bins_size = lsv_bins.length;
    var num_copies;

    lsv_bins.forEach(function (b, i) {
        num_copies = Math.round(num_samples * b);
        tmp_bins = tmp_bins.concat(new Array(num_copies).fill((1 / bins_size) / 2 + (i / bins_size)))
    });

    return tmp_bins
}

function splice_graphs(json, sampleNames) {
    var sg = $('#splice-graphs');
    sg.empty();
    json.forEach(function (spliceGraph, index) {
        var svg = $('<svg class="splice-graph u-full-width">').height(250).width(sg.width());
        new SpliceGraph({svg: svg, data: spliceGraph}).draw();
        sg.append($('<div>').text(sampleNames[index].toUpperCase()).addClass('title'));
        sg.append(svg);
    })
}

function format(d) {
    return '<div class="slider">' +

        '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;margin:0;">' +

        '<tr>' +
        '<td data-tooltip="The nucleotide length of the primer" >Primer Length:</td>' +
        '<td>' + d.primer_length + '</td>' +
        '</tr>' +

        '<tr>' +
        '<td data-tooltip="The melting temperature estimate of the primer">T<sub>m</sub>:</td>' +
        '<td>' + d.Tm + '</td>' +
        '</tr>' +

        '<tr>' +
        '<td data-tooltip="Number of G and C nucleotides">GCs:</td>' +
        '<td>' + d.number_of_gcs + '</td>' +
        '</tr>' +

        '<tr>' +
        '<td data-tooltip="The ratio of G and C nucleotides to total primer length">GC Content:</td>' +
        '<td>' + d.gc_content.toFixed(3) + '</td>' +
        '</tr>' +

        '<tr>' +
        '<td  data-tooltip="The number of G and C nucleotides at the 3’ end of the primer">End GCs:</td>' +
        '<td>' + d.count_end_gcs + '</td>' +
        '</tr>' +

        '</table>' +
        '</div>';
}

$.fn.dataTable.ext.search.push(
    function (settings, data, dataIndex) {
        if (settings.sTableId.startsWith('primers')) {
            var rtnValue;
            var modifier = $('input[name=modifier]:checked').val();
            ['primer-length', 'tm', 'gcs', 'gc-content', 'end-gcs'].forEach(function (value, index) {
                var v = parseFloat($('.filter.' + value).val().trim());
                if (!isNaN(v)) {
                    var expression = [parseFloat(data[index + 2]).toFixed(3), modifier, v.toFixed(3)].join(' ');
                    if (rtnValue === undefined)
                        rtnValue = eval(expression);
                    else
                        rtnValue = rtnValue && eval(expression)
                }
            });
            if (rtnValue !== undefined)
                return rtnValue;
        }
        return true;
    }
);
