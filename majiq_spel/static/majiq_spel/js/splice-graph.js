var SpliceGraph = function (opts) {
    var svg = opts.svg;
    this.data = opts.data;
    this.svgHeight = svg.height();
    this.svgWidth = svg.width();
    this.svgContainer = d3.selectAll(svg);
    this.exonHeight = 30;
    this.strokeWidth = 2;
    this.dashArray = '4,2';

    var numberOfSections = this.data.exons.reduce(function (accu, exon) {
        return Math.max(exon.section_index, accu)
    }, 0) + 1;
    var numberOfIntrons = this.data.exons.reduce(function (accu, exon) {
        return Math.max(exon.exon_index, accu)
    }, 0);
    this.sectionWidth = 40;
    this.intronWidth = (this.svgWidth - (numberOfSections * this.sectionWidth)) / numberOfIntrons;

    if (this.intronWidth < 20) {
        this.sectionWidth = (-((20 * numberOfIntrons) - this.svgWidth)) / numberOfSections;
        this.intronWidth = (this.svgWidth - (numberOfSections * this.sectionWidth)) / numberOfIntrons
    }

    this.spliceGraph = this.svgContainer.append('g')
        .classed('splice-graph', true)
        .attr('transform', 'translate(0,' + this.svgHeight + ') scale(1, -1)');
};


SpliceGraph.prototype.draw = function () {
    this.initJunctions();
    this.drawJunctions();
    this.drawIntrons();
    this.drawExons();
    this.drawIntronRetentionReads();
    this.drawIntronRetentionLines();
    this.drawExons();
    this.junctionForce();
};

SpliceGraph.prototype.initJunctions = function () {

    var sg = this;

    sg.data.junctions.forEach(function (j) {
        if (sg.data.backwards) {
            j.end_section_index++;
            j.start = (j.end_section_index * sg.sectionWidth) + (j.end_exon_index * sg.intronWidth);
            j.end = (j.start_section_index * sg.sectionWidth) + (j.start_exon_index * sg.intronWidth);
        }
        else {
            j.start_section_index++;
            j.start = (j.start_section_index * sg.sectionWidth) + (j.start_exon_index * sg.intronWidth);
            j.end = (j.end_section_index * sg.sectionWidth) + (j.end_exon_index * sg.intronWidth);
        }

        j.y = ((j.end - j.start) + 30) * .2;
        j.x = j.start + ((j.end - j.start) / 2);
    })

};

SpliceGraph.prototype.drawJunctions = function () {

    var sg = this;

    var junctions = sg.spliceGraph.append('g')
        .classed('junctions', true)
        .selectAll('.junction-group')
        .data(sg.data.junctions);

    var junction = junctions
        .enter()
        .filter(function (d) {
            return !d.intron_retention
        })
        .append('g')
        .classed('junction', true);

    junction
        .append('text')
        .classed('reads', true)
        .attr('transform', 'translate(0,' + sg.svgHeight + ') scale(1, -1)')
        .attr('text-anchor', 'middle')
        .attr('font-size', 10)
        .attr('font-family', 'sans-serif')
        .attr('x', function (j) {
            return j.start + (j.end - j.start) / 2;
        })
        .text(function (j) {
            return j.reads
        });

    junction
        .append('path')
        .classed('edge', true)
        .attr('fill', 'None')
        .attr('fill-opacity', 0)
        .attr('stroke', function (d) {
            if (d.color)
                return d.color;
            else
                return '#b7b7b7';
        })
        .attr('stroke-width', sg.strokeWidth)
        .attr('stroke-dasharray', function (d) {
            if (!d.color)
                return sg.dashArray
        });
};

SpliceGraph.prototype.drawIntrons = function () {

    var sg = this;

    var introns = sg.spliceGraph.append('g')
        .classed('introns', true)
        .selectAll('.intron')
        .data(sg.data.junctions);

    introns
        .enter()
        .filter(function (d) {
            return d.intron_retention
        })
        .append('rect')
        .classed('intron', true)
        .classed('intron-retention', true)
        .attr('fill', function (d) {
            if (d.color)
                return d.color;
            else
                return 'none'
        })
        .attr('stroke', function (d) {
            if (d.color)
                return d.color;
            else
                return '#b7b7b7';
        })
        .attr('stroke-width', sg.strokeWidth)
        .attr('stroke-dasharray', function (d) {
            if (!d.color)
                return sg.dashArray
        })
        .attr('x', function (d) {
            return d.start
        })
        .attr('y', (sg.strokeWidth / 2) + (sg.exonHeight / 3) - (sg.exonHeight / 8))
        .attr('width', function (d) {
            return d.end - d.start
        })
        .attr('height', sg.exonHeight / 4);
};

SpliceGraph.prototype.drawIntronRetentionReads = function () {

    var sg = this;

    var intronRetentionReads = sg.spliceGraph.append('g')
        .classed('intron-retention-reads', true)
        .selectAll('.intron-reads')
        .data(sg.data.junctions);

    intronRetentionReads
        .enter()
        .filter(function (d) {
            return d.intron_retention
        })
        .append('text')
        .attr('transform', function () {
            var y = (sg.strokeWidth / 2) + ((sg.exonHeight * 5) / 6) - 3;
            return 'translate(0,' + y + ') scale(1,-1)'
        })
        .attr('text-anchor', function (d) {
            if (d.intron_retention === 'start')
                return 'end';
            else
                return 'start'
        })
        .attr('font-size', 10)
        .attr('font-family', 'sans-serif')
        .attr('x', function (d) {
            if (d.intron_retention === 'start')
                return d.end - 20;
            else
                return d.start + 20

        })
        .text(function (d) {
            return d.reads
        });
};

SpliceGraph.prototype.drawIntronRetentionLines = function () {

    var sg = this;

    var intronRetentionLines = sg.spliceGraph.append('g')
        .classed('intron-retention-lines', true)
        .selectAll('.intron-retention-line')
        .data(sg.data.junctions);

    intronRetentionLines
        .enter()
        .filter(function (d) {
            return d.intron_retention
        })
        .append('line')
        .attr('stroke', function (d) {
            if (d.line_color)
                return d.line_color;
            else
                return '#b7b7b7';
        })
        .attr('stroke-width', sg.strokeWidth)
        .attr('stroke-dasharray', function (d) {
            if (!d.line_color)
                return sg.dashArray
        })
        .attr('x1', function (d) {
            if (d.intron_retention === 'start')
                return d.end;
            else
                return d.start
        })
        .attr('x2', function (d) {
            if (d.intron_retention === 'start')
                return d.end - 15;
            else
                return d.start + 15
        })
        .attr('y1', function () {
            return (sg.strokeWidth / 2) + ((sg.exonHeight * 5) / 6)
        })
        .attr('y2', function () {
            return (sg.strokeWidth / 2) + ((sg.exonHeight * 5) / 6)
        });

};

SpliceGraph.prototype.drawExons = function () {

    var sg = this;

    var exons = sg.spliceGraph.append('g')
        .classed('exons', true)
        .selectAll('.exon')
        .data(sg.data.exons);

    exons
        .enter()
        .append("rect")
        .classed('exon', true)
        .classed('intron-retention', function (d) {
            return d.intron_retention
        })
        .attr('fill', function (d) {
            if (d.color)
                return d.color;
            else
                return 'none'
        })
        .attr('stroke', function (d) {
            if (d.color)
                return d.color;
            else
                return '#b7b7b7';
        })
        .attr('stroke-width', sg.strokeWidth)
        .attr('stroke-dasharray', function (d) {
            if (!d.color)
                return sg.dashArray
        })
        .attr('x', function (d) {
            return (d.section_index * sg.sectionWidth) + (d.exon_index * sg.intronWidth)
        })
        .attr('height', function (d) {
            if (d.intron_retention)
                return (d.y_size * sg.exonHeight) / 4;
            else
                return d.y_size * sg.exonHeight;
        })
        .attr('y', function (d) {
            if (d.intron_retention)
                return (sg.strokeWidth / 2) + (sg.exonHeight / 3) - (sg.exonHeight / 8);
            else
                return (d.y_index * d.y_size * sg.exonHeight) + sg.strokeWidth / 2;
        })
        .attr('width', function () {
            return sg.sectionWidth
        });
};

SpliceGraph.prototype.junctionForce = function () {

    var sg = this;

    d3.forceSimulation(sg.data.junctions)
        .force('charge', d3.forceManyBody().strength(-15))
        .on('tick', function () {
            sg.updateJunctions();
            sg.updateReads();
        });

};


SpliceGraph.prototype.updateJunctions = function () {

    var sg = this;

    sg.svgContainer.selectAll('.edge')
        .attr('d', function (j) {
            j.y = Math.max(25, Math.min(200, j.y));
            var m = 'M' + [j.start, sg.exonHeight].join(',');
            var c1 = 'C' + [j.start, j.y + sg.exonHeight].join(',');
            var c2 = [j.end, j.y + sg.exonHeight].join(',');
            var e = [j.end, sg.exonHeight].join(',');
            return [m, c1, c2, e].join(' ');
        });
};

SpliceGraph.prototype.updateReads = function () {

    var sg = this;

    sg.svgContainer.selectAll('.reads')
        .attr('y', function (j) {
            return (.75 * (sg.svgHeight - j.y )) + 25
        });
};


