var Isoform = function (opts) {
    /**
     * Used to draw isoform cartoons in SVG elements.
     */

    /*
     Class constants
     */
    this.exonWidth = 50;
    this.exonHeight = 25;
    this.intron = 4;
    this.lengthWidth = 50;
    this.fontSize = 12;
    this.strokeWidth = 2;

    /*
     Save values from opts as class variables.
     */
    this.data = opts.data;
    this.primer = opts.primer;
    this.element = d3.selectAll(opts.element)
        .attr('height', this.exonHeight + this.strokeWidth)
        .attr('width', (this.data.exons.length * (this.intron + this.exonWidth)) + this.lengthWidth);
};

Isoform.prototype.draw = function () {
    /**
     * Draw isoform cartoon
     */
    var isoform = this;
    isoform.data.exons.forEach(function (exon, index) {
        isoform.drawExon(exon, index)
    });
    isoform.drawLength()
};

Isoform.prototype.drawExon = function (exon, exonIndex) {
    /**
     * Draw an exon.
     */
    var isoform = this;
    var sectionWidth = this.exonWidth / exon.sections.length;
    var height = this.exonHeight;
    var y = 1;

    if (exon.intron_retention) {
        y = this.exonHeight / 3;
        height = this.exonHeight / 3;
    }

    exon.sections.forEach(function (section, sectionIndex) {
        isoform.element
            .append('rect')
            .attr('x', isoform.lengthWidth + ((isoform.exonWidth + isoform.strokeWidth) * exonIndex) + (isoform.intron * exonIndex) + (sectionWidth * sectionIndex) + 1)
            .attr('y', y)
            .attr('width', sectionWidth)
            .attr('height', height)
            .attr('fill', function () {
                if (section.color)
                    return section.color;
                else
                    return 'None'
            })
            .attr('stroke', function () {
                if (section.color)
                    return section.color;
                else
                    return '#b7b7b7';
            })
            .attr('stroke-width', isoform.strokeWidth)
            .attr('stroke-dasharray', function () {
                if (!section.color)
                    return '4,2'
            })
    })
}
;

Isoform.prototype.drawLength = function () {
    /**
     * draw length value in SVG.
     */
    var isoform = this;
    this.element
        .append('text')
        .text(function () {
            if (isoform.getLength())
                return isoform.getLength() + ' bp'
        })
        .attr('x', 0)
        .attr('y', (this.exonHeight / 2) + (this.fontSize / 2))
        .attr('font-size', this.fontSize)
};

Isoform.prototype.getLength = function () {
    /**
     * Return length value for this isoform.
     */
    try {
        return this.primer[this.data.id];
    } catch (TypeError) {
        return this.data.length
    }
};