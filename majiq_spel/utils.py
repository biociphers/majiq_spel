import json
import os
import sys

from colour import Color

from majiq_spel.constants import HEX_COLORS, DEBUG, LSV_TEXT_VERSION, COLOR_ORDER


class LSVTextException(Exception):
    """
    Error when parsing LSV text. 
    """

    def __init__(self, message):
        if DEBUG:
            super(LSVTextException, self).__init__(message)
        else:
            self.stderr(message)

    def stderr(self, message):
        sys.stderr.write(message)
        sys.exit(self)


class NotJsonText(LSVTextException):
    """
    Error when text can't be converted to JSON. 
    """

    def __init__(self):
        message = 'The copied LSV text cannot be read.  It is possible the copied text isn\'t complete.  Try copying ' \
                  'the LSV text again and resubmit.'
        super(NotJsonText, self).__init__(message)


class EmptyJsonText(LSVTextException):
    """
    Error when attempt to convert text to json, but is empty string. 
    """

    def __init__(self):
        message = 'Unable to generate MAJIQ-SPEL with no LSV text.'
        super(EmptyJsonText, self).__init__(message)


class RequiredFieldNotFound(LSVTextException):
    """
    Error when required field is not found in LSV text. 
    """

    def __init__(self, field):
        message = 'Could not find required field "{0}" in LSV text.'.format(field)
        super(RequiredFieldNotFound, self).__init__(message)


class InvalidLsvVersion(LSVTextException):
    """
    Error when LSV version is out of date. 
    """

    def __init__(self):
        message = 'This LSV text is out of date.  Verify that Voila and MAJIQ-SPEL are up to date and try again.'
        super(InvalidLsvVersion, self).__init__(message)


def check_fields(d, fields):
    """
    Check fields in LSV text. 
    :param d: dictionary
    :param fields: fields list. 
    :return: 
    """
    for field in fields:
        if field not in d or not d[field]:
            RequiredFieldNotFound(field)


def validate(raw_data):
    """
    Validate raw data from command line. 
    :param raw_data: raw data string. 
    :return: 
    """
    try:
        if not raw_data:
            raise EmptyJsonText()
        try:
            raw_data_dict = json.loads(raw_data)
        except ValueError:
            raw_data_dict = json.loads(raw_data.replace('\\', ''))
    except ValueError:
        raise NotJsonText()

    if 'lsv_text_version' not in raw_data_dict or str(raw_data_dict['lsv_text_version']) != str(LSV_TEXT_VERSION):
        raise InvalidLsvVersion()

    for field in ['lsv', 'splice_graphs', 'lsv_text_version', 'genome', 'sample_names']:
        if field not in raw_data_dict:
            raise LSVTextException('Missing require field: ' + field)

    for field in ['exons', 'junctions', 'lsv_type', 'lsv_id', 'name']:
        if field not in raw_data_dict['lsv']:
            raise LSVTextException('LSV is missing field: ' + field)

    for splice_graph in raw_data_dict['splice_graphs']:
        for field in ['junctions', 'exons']:
            if field not in splice_graph:
                raise LSVTextException('Splice Graph is missing field: ' + field)

    return raw_data_dict


def is_hex_color(color):
    """
    Is this color value actually in hex?
    :param color: color value. 
    :return: 
    """
    return color and color.startswith('#')


def darken_color(c):
    """
    darken color slightly.
    :param c: color object
    :return: 
    """

    if c:
        if not is_hex_color(c):
            c = HEX_COLORS[c]

        color = Color(c)
        color.set_luminance(color.get_luminance() * .9)
        c = color.get_hex()
    return c


def darken_colors(cs):
    """
    Darken list of colors
    :param cs: list of colors
    :return: 
    """
    return [darken_color(c) for c in cs]


def is_plus_strand(lsv_dict):
    """
    Is this lsv plus strand?    
    :param lsv_dict: lsv dict 
    :return: boolean 
    """
    return lsv_dict['strand'] == '+'


def is_minus_strand(lsv_dict):
    """
    is this lsv minus strand
    :param lsv_dict: lsv dict
    :return: 
    """
    return not is_plus_strand(lsv_dict)


def is_source(lsv_dict):
    """
    is this lsv source type?
    :param lsv_dict: lsv dict
    :return: 
    """
    return lsv_dict['lsv_type'].startswith('s')


def is_target(lsv_dict):
    """
    is this lsv target type?
    :param lsv_dict: lsv dict
    :return: 
    """
    return not is_source(lsv_dict)


def scale(domain, range):
    """
    Scale function
    :param domain: domain list 
    :param range: range list
    :return: 
    """
    a = range[1] - range[0]
    b = float(domain[1] - domain[0])

    def _scale(value):
        return (((value - domain[0]) * a) / b) + range[0]

    return _scale


def reverse_sg(splice_graphs):
    """
    Invert splice graph
    :param splice_graphs: splice graphs list 
    :return: 
    """
    max_exon_index = max(e['exon_index'] for e in splice_graphs[0]['exons'])
    min_exon_index = min(e['exon_index'] for e in splice_graphs[0]['exons'])
    reverse_exon_index = scale((min_exon_index, max_exon_index), (max_exon_index, min_exon_index))

    max_section_index = max(e['section_index'] for e in splice_graphs[0]['exons'])
    min_section_index = min(e['section_index'] for e in splice_graphs[0]['exons'])
    reverse_section_index = scale((min_section_index, max_section_index), (max_section_index, min_section_index))

    for splice_graph in splice_graphs:

        for exon in splice_graph['exons']:
            exon['exon_index'] = reverse_exon_index(exon['exon_index'])
            exon['section_index'] = reverse_section_index(exon['section_index'])
            if exon['intron_retention']:
                if exon['intron_retention'] == 'start':
                    exon['intron_retention'] = 'end'
                elif exon['intron_retention'] == 'end':
                    exon['intron_retention'] = 'start'
                else:
                    raise Exception('unknown intron_retention value')

        for junc in splice_graph['junctions']:
            junc['end_exon_index'] = reverse_exon_index(junc['end_exon_index'])
            junc['start_exon_index'] = reverse_exon_index(junc['start_exon_index'])
            junc['end_section_index'] = reverse_section_index(junc['end_section_index'])
            junc['start_section_index'] = reverse_section_index(junc['start_section_index'])
            if junc['intron_retention']:
                if junc['intron_retention'] == 'start':
                    junc['intron_retention'] = 'end'
                elif junc['intron_retention'] == 'end':
                    junc['intron_retention'] = 'start'
                else:
                    raise Exception('unknown intron_retention value')


def reverse_lsv(lsv_dict):
    """
    Invert LSV.
    :param lsv_dict: lsv dict 
    :return: 
    """
    for splice_graph in lsv_dict['splice_graphs']:
        exons = splice_graph['exons']
        juncs = splice_graph['junctions']

        for exon in exons:
            a5 = exon['a5']
            a3 = exon['a3']
            start = exon['start']
            end = exon['end']

            a3.reverse()
            a5.reverse()

            exon['a5'] = a3
            exon['a3'] = a5
            exon['start'] = end
            exon['end'] = start

        exons.reverse()

        for junc in juncs:
            start = junc['start']
            end = junc['end']

            junc['end'] = start
            junc['start'] = end


def reverse_isoform(isoform_table):
    """
    Reverse isoform. 
    :param isoform_table: isoform table list
    :return: 
    """
    for row in isoform_table:
        for isoform in row['isoforms']:
            exons = isoform['exons']
            exons.reverse()
            for exon in exons:
                exon['sections'].reverse()


def convert_colors_to_hex(splice_graphs):
    """
    
    :param splice_graphs: 
    :return: 
    """
    for splice_graph in splice_graphs:
        for junction in splice_graph['junctions']:
            if junction['color'] and not is_hex_color(junction['color']):
                junction['color'] = Color(HEX_COLORS[junction['color']]).get_hex()

        for exon in splice_graph['exons']:
            if exon['color'] and not is_hex_color(exon['color']):
                exon['color'] = Color(HEX_COLORS[exon['color']]).get_hex()


def write_json(json_dict, args, json_filename):
    json_path = os.path.join(args.output_dir, 'json')
    if not os.path.isdir(json_path):
        os.makedirs(json_path)

    with open(os.path.join(json_path, json_filename), 'w') as j:
        j.write(json.dumps(json_dict, indent=2))


def read_json(args, json_filename):
    json_path = os.path.join(args.output_dir, 'json')
    with open(os.path.join(json_path, json_filename), 'r') as j:
        json_dict = json.loads(j.read())
    return json_dict


def round_index(index):
    index = round(index, 3)
    if int(index) == index:
        return int(index)
    return index


def should_reverse_lsv_dict(lsv_dict):
    return (is_target(lsv_dict) and is_plus_strand(lsv_dict)) or (is_source(lsv_dict) and is_minus_strand(lsv_dict))


def should_reverse_isoform(lsv_dict):
    return (is_target(lsv_dict) and is_minus_strand(lsv_dict)) or (is_plus_strand(lsv_dict) and is_target(lsv_dict))


def get_color(color_index):
    if color_index is None:
        return None

    if color_index == -1:
        color = 'grey'
    else:
        color = COLOR_ORDER[color_index % len(COLOR_ORDER)]

    return HEX_COLORS[color]
