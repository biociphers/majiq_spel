from majiq_spel import utils
from majiq_spel.utils import reverse_lsv


def get_intron_retention(exon, splice_sites):
    """
    Check if this is intron retention and if it's the start or end of it. 
    :param exon: our exon
    :param splice_sites: splice sites
    :return: 
    """
    # if exon['intron_retention']:
    #     if splice_sites['start']:
    #         return 'start'
    #     else:
    #         return 'end'
    return False


def exons(splice_graph, reverse):
    """
    Parse exons from splice graph to be drawn with d3. 
    :param splice_graph: splice graph dict
    :param reverse: are we drawing this in a reverse direction. 
    :return: 
    """
    exons = splice_graph['exons']
    juncs = splice_graph['junctions']
    colors = [utils.get_color(-1)]
    sections = []
    section_index = 0

    for exon_index, exon in enumerate(exons):

        splice_sites_dict = {}

        for end_index in exon['a3']:
            try:
                splice_sites_dict[juncs[end_index]['end']]['end'].append(end_index)
            except KeyError:
                splice_sites_dict[juncs[end_index]['end']] = {'end': [end_index], 'start': []}

        for start_index in exon['a5']:
            try:
                splice_sites_dict[juncs[start_index]['start']]['start'].append(start_index)
            except KeyError:
                splice_sites_dict[juncs[start_index]['start']] = {'start': [start_index], 'end': []}

        repeat_juncs = set(a3 for a3 in exon['a3'] for a5 in exon['a5'] if a5 == a3)

        for coordinate, splice_sites in sorted(iter(splice_sites_dict.items()), reverse=reverse):

            if splice_sites['end']:
                colors = [utils.get_color(juncs[j]['color_index']) for j in splice_sites['end'] if
                          'color_index' in juncs[j]]

                if not colors:
                    colors = [None]

            for index, color in enumerate(colors):
                s = {
                    'y_size': 1.0 / len(colors),
                    'y_index': index,
                    'exon_index': exon_index,
                    'section_index': section_index,
                    'junc_start': splice_sites['start'],
                    'junc_end': splice_sites['end'],
                    'color': color,
                    'intron_retention': get_intron_retention(exon, splice_sites)
                }

                sections.append(s)

            if splice_sites['start']:
                colors = utils.darken_colors(colors)

            section_index += 1

            if repeat_juncs.intersection(splice_sites['start']):
                for index, color in enumerate(colors):
                    s = {
                        'y_size': 1,
                        'y_index': 0,
                        'exon_index': exon_index,
                        'section_index': section_index,
                        'junc_start': [],
                        'junc_end': [],
                        'color': color,
                        'intron_retention': get_intron_retention(exon, splice_sites)
                    }
                    sections.append(s)
                section_index += 1

        colors = [None]

    return sections


def junctions(splice_graph, sections):
    """
    Parse junctions from splice graph to be drawn using d3.
    :param splice_graph: splice graph dict
    :param sections: exon sections
    :return: 
    """
    juncs = [{'intron_retention': False} for _ in range(len(splice_graph['junctions']))]

    for section in sections:

        for junc_index in section['junc_start']:
            junc = juncs[junc_index]
            junc['start_section_index'] = section['section_index']
            junc['start_exon_index'] = section['exon_index']
            junc['reads'] = splice_graph['junctions'][junc_index]['reads']
            junc['intron_retention'] = section['intron_retention'] or junc['intron_retention']

            try:
                junc['color'] = utils.get_color(splice_graph['junctions'][junc_index]['color_index'])
            except KeyError:
                junc['color'] = None

        for junc_index in section['junc_end']:
            junc = juncs[junc_index]
            junc['end_section_index'] = section['section_index']
            junc['end_exon_index'] = section['exon_index']
            junc['intron_retention'] = section['intron_retention'] or junc['intron_retention']

    return juncs


def visualize_sg(lsv_dict):
    """
    Using the lsv dict, parse exons and juctions to be drawn in d3. 
    :param lsv_dict: 
    :return: 
    """
    splice_graphs = []

    reverse = utils.should_reverse_lsv_dict(lsv_dict)
    if reverse:
        reverse_lsv(lsv_dict)

    for splice_graph in lsv_dict['splice_graphs']:
        sections = exons(splice_graph, reverse)
        splice_graphs.append({
            'exons': sections,
            'junctions': junctions(splice_graph, sections)
        })

    return splice_graphs


def intron_retention(splice_graphs):
    """
    Add color to intron retention.
    :param splice_graphs: splice graph dictionary
    :return: 
    """
    for splice_graph in splice_graphs:
        exons = splice_graph['exons']
        juncs = splice_graph['junctions']

        colors = {}
        for exon in exons:
            if exon['intron_retention']:
                for junc_index in exon['junc_end']:
                    color = juncs[junc_index]['color']
                    juncs[junc_index]['line_color'] = color
                    colors[exon['exon_index']] = color

                for junc_index in exon['junc_start']:
                    juncs[junc_index]['line_color'] = juncs[junc_index]['color']
                    try:
                        juncs[junc_index]['color'] = colors[exon['exon_index']]
                    except KeyError:
                        pass

            del exon['junc_start']
            del exon['junc_end']
