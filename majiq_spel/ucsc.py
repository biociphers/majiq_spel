import gzip
import os
import re
import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
from xml.etree import ElementTree

from colour import Color

from majiq_spel import constants
from majiq_spel import utils
from majiq_spel.utils import is_minus_strand


class NoUcscDnaData(Exception):
    """
    Error when no DNA data was retrieved from UCSC.
    """
    pass


class ScrewyStartAndEndExons(Exception):
    """
    Error when start and end exons don't pass sanity check.
    """
    pass


def add_start_end_location_forward(forward_primer, is_plus_strand, juncs):
    """
    Add start and end loction to forward primers. 
    :param forward_primer: forward primer dict
    :param is_plus_strand: is this plus strand
    :param juncs: junctions list
    :return: 
    """
    primer_length = forward_primer['primer_length']
    frag_length = forward_primer['fragment_length']

    last_junc = max(j['start'] for j in juncs)

    if is_plus_strand:
        start = juncs[0]['start'] - frag_length
        end = start + primer_length
    else:
        end = last_junc + frag_length - 1
        start = end - primer_length

    forward_primer['start'] = start
    forward_primer['end'] = end


def add_start_end_location_reverse(reverse_primer, is_plus_strand, juncs):
    """
    Add start and end locations to reverse primers. 
    :param reverse_primer: reverse primer dict
    :param is_plus_strand: is this plus strand
    :param juncs: junction list
    :return: 
    """
    primer_length = reverse_primer['primer_length']
    frag_length = reverse_primer['fragment_length']

    last_junc = max(j['end'] for j in juncs)

    if is_plus_strand:
        start = last_junc + frag_length - primer_length - 1
        end = start + primer_length
    else:
        start = juncs[0]['end'] - frag_length
        end = start + primer_length

    reverse_primer['start'] = start
    reverse_primer['end'] = end


def add_start_end_location(primer_pairs, lsv_dict):
    """
    Add start and end locations to primers. 
    :param primer_pairs: primer pairs dict
    :param lsv_dict: lsv dictionary 
    :return: 
    """
    juncs = lsv_dict['splice_graphs'][0]['junctions']
    is_plus_strand = utils.is_plus_strand(lsv_dict)
    for primer in primer_pairs:
        add_start_end_location_forward(primer['forward'], is_plus_strand, juncs)
        add_start_end_location_reverse(primer['reverse'], is_plus_strand, juncs)


def compliment_dna(dna):
    """
    Return complimented dna string.
    :param dna: dna string
    :return:
    """
    compliment = {'C': 'G', 'T': 'A', 'G': 'C', 'A': 'T'}
    return ''.join([compliment[x.upper()] for x in dna])


def reverse_dna(dna):
    """
    Invert dna string.
    :param dna: dna string
    :return:
    """
    return dna[::-1]


def all_going_same_direction(exons):
    if not (all(exon['expressed_start'] / float(exon['expressed_end']) < 1 for exon in exons) or all(
                    exon['expressed_start'] / float(exon['expressed_end']) > 1 for exon in exons)):
        raise ScrewyStartAndEndExons()

def fix_half_exon(exon):
    if exon['expressed_start'] == -1:
        exon['expressed_start'] = exon['expressed_end'] - 1
    if exon['expressed_end'] == -1:
        exon['expressed_end'] = exon['expressed_start'] + 1
    #return exon

def get_dna(lsv_dict):
    """
    Get nucleotide information from UCSC.
    :param lsv_dict: dictionary containing lsv data
    :return:
    """
    exons = lsv_dict['splice_graphs'][0]['exons']
    chr = lsv_dict['chromosome']
    db = lsv_dict['genome']

    all_going_same_direction(exons)

    fix_half_exon(exons[-1])
    fix_half_exon(exons[0])

    url = 'http://genome.ucsc.edu/cgi-bin/das/{0}/dna'.format(db)
    values = [('segment', '{0}:{1},{2}'.format(chr, e['expressed_start'], e['expressed_end'])) for e in
              (exons[0], exons[-1])]

    data = urllib.parse.urlencode(values)

    url = url + '?' + data

    with urllib.request.urlopen(url) as response:
        response_text = response.read()
        response_code = response.getcode()


    if not response_text and response_code == 200:
        raise NoUcscDnaData()

    dasdna = ElementTree.fromstring(response_text)

    exons_dna = (dna.text.replace('\n', '') for sequence in dasdna for dna in sequence)

    if is_minus_strand(lsv_dict):
        exons_dna = (compliment_dna(reverse_dna(dna)) for dna in exons_dna)

    return exons_dna


class AnnotationFile(object):
    def __init__(self, lsv_dict, filename):
        """
        Create an annotation file in BED format.  This file will be transfered to UCSC Genome Browser for
        visualization.
        :param lsv_dict: dictionary with lsv data
        :param filename: location of annotation file.
        """
        self.chr = lsv_dict['chromosome']
        self.exons = lsv_dict['splice_graphs'][0]['exons']
        self.juncs = lsv_dict['splice_graphs'][0]['junctions']
        self.start = self.exons[0]['start']
        self.end = self.exons[-1]['end']
        self.strand = lsv_dict['strand']
        self.lsv_type = lsv_dict['lsv_type']
        self.lsv_dict = lsv_dict

        self.filename = filename
        if not os.path.isdir(os.path.dirname(filename)):
            os.makedirs(os.path.dirname(filename))

        self.fields = ['chrom', 'chromStart', 'chromEnd', 'name', 'score', 'strand', 'thickStart', 'thickEnd',
                       'itemRgb', 'blockCount', 'blockSizes', 'blockStarts']

        self.defaults = {
            'score': 0,
            'chrom': self.chr,
            'strand': self.strand
        }

        self.output = [('browser', 'position', '{0}:{1}-{2}'.format(self.chr, self.start, self.end)), ]

    def add_track(self, **kwargs):
        """
        Add track line to bed file.
        :param kwargs: key word arguments
        :return:
        """

        t = ['track']
        kwargs['itemRgb'] = 'On'
        for key in kwargs:
            if re.search(r'\s', str(kwargs[key])):
                t.append('{0}="{1}"'.format(key, kwargs[key]))
            else:
                t.append('{0}={1}'.format(key, kwargs[key]))

        self.output.append(t)

    def add_bed_line(self, **kwargs):
        """
        Add line to bed file.
        :param kwargs: key word arguments
        :return:
        """
        kwargs.update(self.defaults)
        kwargs['thickStart'] = kwargs['chromStart']
        kwargs['thickEnd'] = kwargs['chromEnd']
        itemRgb = [int(rgb * 255) for rgb in Color(kwargs['itemRgb']).get_rgb()]
        if sum(itemRgb) == 0:
            kwargs['itemRgb'] = 0
        else:
            kwargs['itemRgb'] = ','.join(str(x) for x in itemRgb)

        self.output.append(str(kwargs[field]) for field in self.fields)

    def add_exons(self):
        """
        Add exons to bed file.
        :return:
        """
        self._add_exons(False)

    def name_junctions(self):
        """
        Add names to junctions. 
        :return: 
        """
        exons = self.exons
        j = {}

        for exon_index, exon in enumerate(exons):
            splice_sites_a3 = sorted(set(self.juncs[junc_index]['end'] for junc_index in exon['a3']))
            splice_sites_a5 = sorted(set(self.juncs[junc_index]['start'] for junc_index in exon['a5']))

            for junc_index in exon['a3']:
                if junc_index not in j:
                    j[junc_index] = {}

                splice_site_index = splice_sites_a3.index(self.juncs[junc_index]['end'])
                j[junc_index]['a3'] = 'e{0}.{1}o{2}'.format(exon_index + 1, splice_site_index + 1, len(splice_sites_a3))

            for junc_index in exon['a5']:
                if junc_index not in j:
                    j[junc_index] = {}

                splice_site_index = splice_sites_a5.index(self.juncs[junc_index]['start'])
                j[junc_index]['a5'] = 'e{0}.{1}o{2}'.format(exon_index + 1, splice_site_index + 1, len(splice_sites_a5))

        for key, value in list(j.items()):
            self.juncs[key]['name'] = value['a3'] + value['a5']

    def add_junctions(self):
        """
        Add junctions to the bed file.
        :return:
        """
        self.name_junctions()

        for index, junc in enumerate(self.juncs):
            # if not junc['intron_retention']:
            start_end = sorted([junc['start'], junc['end']])

            try:
                color = utils.get_color(junc['color_index'])
            except KeyError:
                color = 'black'

            self.add_bed_line(
                chromStart=start_end[0],
                chromEnd=start_end[1],
                name=junc['name'],
                itemRgb=color,
                blockStarts=','.join(str(x) for x in (0, abs(junc['end'] - junc['start']) - 1)),
                blockSizes=','.join(str(x) for x in (1, 1)),
                blockCount=2,
            )

    def write_output(self):
        """
        Write bed file to gzip file.
        NOTE: RHEL 6 python 2.6 doesn't support gzip 'with' boxes.
        :return:
        """

        content = '\n'.join(' '.join(line) for line in self.output)
        gz = gzip.open(self.filename, 'wb')
        gz.write(content.encode())
        gz.close()
        if constants.DEBUG:
            with open(os.path.join(os.path.dirname(self.filename), 'annoatation.txt'), 'w') as a:
                a.write(content)

    def add_forward_primers(self, forward_primers):
        """
        Add forward primers.
        :param forward_primers: forward primers dictionary 
        :return: 
        """
        for _, primer in list(forward_primers.items()):
            self.add_bed_line(
                name=primer['index'],
                chromStart=primer['start'],
                chromEnd=primer['end'],
                itemRgb='black',
                blockCount=1,
                blockSizes=primer['primer_length'],
                blockStarts=0
            )

    def add_reverse_primers(self, reverse_primers):
        """
        Add reverse primers.
        :param reverse_primers: reverser primers dictionary 
        :return: 
        """
        for _, primer in list(reverse_primers.items()):
            self.add_bed_line(
                name=primer['index'],
                chromStart=primer['start'],
                chromEnd=primer['end'],
                itemRgb='black',
                blockCount=1,
                blockSizes=primer['primer_length'],
                blockStarts=0
            )

    def add_intron_retention(self):
        """
        Add intron retention. 
        :return: 
        """
        self._add_exons(True)

    def _add_exons(self, intron_retention):
        """
        Add exons. 
        :param intron_retention: is this exon actually intron retention. 
        :return: 
        """
        exons = self.exons

        for index, exon in enumerate(exons):
            # if ((intron_retention and exon['intron_retention']) or
            #         (not intron_retention and not exon['intron_retention'])):
            start_end = sorted([exon['start'], exon['end']])

            self.add_bed_line(
                chromStart=start_end[0],
                chromEnd=start_end[1],
                name='e{0}'.format(index + 1),
                itemRgb=constants.HEX_COLORS['grey'],
                blockStarts='',
                blockSizes='',
                blockCount=''
            )
