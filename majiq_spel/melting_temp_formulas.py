class MeltingTempFormulas(object):
    def __init__(self):
        """
        Map the formulas name to the formula function
        """
        self.funcs = {
            'marmur': {
                'name': 'Marmur',
                'function': self.marmur
            },
            'wallace': {
                'name': 'Wallace',
                'function': self.wallace
            }
        }
        self.default = 'marmur'
        self.xml = ((key, value['name']) for key, value in list(self.funcs.items()))

    def marmur(self, number_of_gcs, primer_length):
        return (4 * number_of_gcs) + (2 * (primer_length - number_of_gcs))

    def wallace(self, number_of_gcs, primer_length):
        return 64.9 + 41 * (number_of_gcs - 16.4) / (primer_length)
