import re

from majiq_spel.melting_temp_formulas import MeltingTempFormulas
from majiq_spel.ucsc import compliment_dna, reverse_dna


def get_all_valid_primer_pairs(c1_c2_seqs,  args, add_exc_len=0):
    """
    Input the sequence of flanking exons (accepts A,C,G,T,a,c,g,t), the length of the alternative exon portion,
    and additional length to be considred for additional exons/fragments that will be in the exclusion product
    (e.g. if the primer falls an additional exon out from the immediate flanks)

    output pairs of primers that conform to the design rules for manual inspection and final selection
    :param c1_seq:
    :param c2_seq:
    :param len_a:
    :param add_exc_len:
    :return:
    """

    c1_c2_seqs = tuple(c1_c2_seqs)

    if not c1_c2_seqs:
        return []

    if len(c1_c2_seqs) != 2:
        raise Exception('The number of sequences is not 2.')

    c1_seq, c2_seq = c1_c2_seqs
    fwds = find_all_primers(c1_seq, 'C1', args)
    revs = find_all_primers(c2_seq, 'C2', args)

    # Get the valid pairs list of tuples
    valid = []
    for fwd in fwds:
        for rev in revs:
            v = validate_pair(fwd, rev, add_exc_len, args)
            if v:
                valid.append(v)

    return valid


def find_all_primers(exon_seq, exon_type, args):
    """
    given single exon sequence and type ('C1' or 'C2') returns a list strings
    of all sutable primers within that exon with the following format:

        primer_sequence|primer_len|num_GC|GCpercent|Tm|3prime_GC_count|fragment_length

    example:

        'CAGGACACAAGCACAGGGTTAGAGG|25|78|.56|2|67'
    :param exon_seq:
    :param exon_type:
    :return:
    """

    primers = []
    exon_seq = exon_seq.upper()

    for n in range(len(exon_seq)):
        for m in range(22, 30):
            p = get_info(exon_seq[n:n + m], exon_type, exon_seq, args)
            if p:
                primers.append(p)

    return primers


def validate_pair(fwd, rev, additional_len, args):
    """

    :param fwd:
    :param rev:
    :param len_a:
    :param additional_len:
    :return:
    """
    # GGAAATGAGGACACCCCAGAAAGAAGAG|28|14|0.5|84|2|57
    # parse primer data:

    # df = [float(x) for x in fwd.split('|')[1:]]
    # dr = [float(x) for x in rev.split('|')[1:]]

    len_exc_limit = args.min_length_exclude_limit
    gcs_ratio_difference_limit = args.gc_content_difference_limit

    # check1: make sure product lengths fit on gel.
    len_exc = fwd['fragment_length'] + rev['fragment_length'] + additional_len


    # maybe this should be a parameter!!!
    if len_exc <= len_exc_limit:
        return

    # check3: check Tm difference
    if abs(fwd['gc_content'] - rev['gc_content']) >= gcs_ratio_difference_limit:
        return

    # check4: check nt resolution
    return {'forward': fwd, 'reverse': rev, 'length_excluded': len_exc}


def get_info(primer, exon_type, exon_seq, args):
    """
    - gcs_ratio min/max
    - primer_length min/max
    - Tm min/max
    - alternate melting temp
    - 3prime_gcs min/max

    :param primer:
    :param exon_type:
    :param exon_seq:
    :return:
    """
    gcs_3prime_min = args.gcs_3prime_min
    gcs_3prime_max = args.gcs_3prime_max
    gcs_content_min = args.gcs_content_min
    gcs_content_max = args.gcs_content_max
    primer_length_min = args.primer_length_min
    primer_length_max = args.primer_length_max
    tm_min = args.tm_min
    tm_max = args.tm_max

    # Length,GC_content,Tm
    primer_length = len(primer)
    number_of_gcs = primer.count('G') + primer.count('C')
    gc_content = float(number_of_gcs) / primer_length

    # Tm is the temperature
    melting_temp_formula = MeltingTempFormulas().funcs[args.melting_temp_formula]['function']
    tm = melting_temp_formula(number_of_gcs, primer_length)

    # fragment length calculation
    start_index = exon_seq.index(primer)

    end_index = start_index + primer_length

    if is_c1(exon_type):
        fragment_length = len(exon_seq[start_index:])

    else:
        fragment_length = len(exon_seq[:end_index])

    # calculate number of consecutive 3' end G/C
    count_end_gcs = get_3prime_gcs(primer, exon_type)

    # print('analyzing primer %s'%primer)
    if count_end_gcs < gcs_3prime_min or count_end_gcs > gcs_3prime_max:
        # print('\tend GC fail: %s'% endGC)
        return None

    elif tm > tm_max or tm < tm_min:
        # print('\tTm fail %s'%tm)
        return None

    elif gc_content > gcs_content_max or gc_content < gcs_content_min:
        if is_c1(exon_type):
            # print('%s|%s|%s|%s|%s|%s|%s' % (primer, pLen, nGC, GCper, Tm, endGC, frag_len))
            pass
        return None

    elif primer_length < primer_length_min or primer_length > primer_length_max:
        return None

    else:
        if is_c2(exon_type):
            primer = reverse_dna(compliment_dna(primer))

        return {'primer': primer, 'primer_length': primer_length, 'number_of_gcs': number_of_gcs,
                'gc_content': gc_content, 'Tm': tm, 'count_end_gcs': count_end_gcs, 'fragment_length': fragment_length}


def is_c1(exon_type):
    assert exon_type in ['C1', 'C2'], 'Exon type not found. Should be C1 for exon where forward primer falls and ' \
                                      'C2 for exon where reverse primer falls.'
    return exon_type == 'C1'


def is_c2(exon_type):
    return not is_c1(exon_type)


def get_3prime_gcs(primer, exon_type):
    """
    Count g's and c's in a primer string.  If this primer is the C1 primer, we'll reverse the order.
    :param primer: string of nucleotides
    :param exon_type: C1 or C2
    :return: count of g's and c's
    """

    if is_c1(exon_type):
        primer = ''.join(reversed(primer))

    m = re.match(r'^([GCgc]*)', primer)
    return len(m.group(0))
