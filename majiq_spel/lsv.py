from functools import reduce
def get_lsv_dict(raw_data_dict):
    """
    Convert raw data from command line to dictionary of lsv data. 
    :param raw_data_dict: command line raw data
    :return: 
    """
    lsv = raw_data_dict['lsv']
    lsv['genome'] = raw_data_dict['genome']
    lsv['sample_names'] = raw_data_dict['sample_names']
    splice_graphs = raw_data_dict['splice_graphs']

    lsv_start = lsv['exons'][0]['start']

    lsv_end = lsv['exons'][-1]['end']

    lsv['splice_graphs'] = []

    del lsv['exons']
    del lsv['junctions']

    for splice_graph in splice_graphs:

        for exon in splice_graph['exons']:

            expressed_start = exon['start']
            expressed_end = exon['end']

            for junc in splice_graph['junctions']:
                if coord_in_exon(junc['end'], exon):
                    expressed_start = max(junc['end'], expressed_start)
                if coord_in_exon(junc['start'], exon):
                    expressed_end = min(junc['start'], expressed_end)

            exon['expressed_start'] = expressed_start
            exon['expressed_end'] = expressed_end

        found_junctions = []
        starts = False
        ends = False

        # exons in the lsv
        found_exons = [e for e in splice_graph['exons'] if e['start'] >= lsv_start and e['end'] <= lsv_end]

        # remove overlapping exons
        found_exons = reduce(overlapping_exons, found_exons, [])

        for junc in splice_graph['junctions']:
            for exon in found_exons:
                if exon['start'] <= junc['start'] <= exon['end']:
                    starts = True
                if exon['start'] <= junc['end'] <= exon['end']:
                    ends = True

            if starts and ends:
                found_junctions.append(junc)

            starts = False
            ends = False

        for exon in found_exons:
            exon['a3'] = []
            exon['a5'] = []
            for junc_index, junc in enumerate(found_junctions):
                if exon['start'] <= junc['start'] <= exon['end']:
                    exon['a5'].append(junc_index)
                if exon['start'] <= junc['end'] <= exon['end']:
                    exon['a3'].append(junc_index)

        lsv['splice_graphs'].append({'exons': found_exons, 'junctions': found_junctions})

    return lsv


def coord_in_exon(coord, exon):
    return exon['start'] <= coord <= exon['end']


def overlapping_exons(exons, exon):
    """
    Filter out overlapping exons. 
    :param exons: list of exons
    :param exon: our exon
    :return: 
    """
    if exons:
        prev = exons[-1]
        if prev['start'] <= exon['start'] and prev['end'] >= exon['end']:
            return exons
        return exons + [exon]
    else:
        return [exon]
