import os
import shutil
from xml.etree import ElementTree

from pkg_resources import get_distribution

from majiq_spel import constants
from majiq_spel.jinja_utils import get_env
from majiq_spel.melting_temp_formulas import MeltingTempFormulas
from majiq_spel.run import get_parser


def install_tool_xml():
    """
    Add this tool to the galaxy tool xml file.
    :return: 
    """
    template = get_env().get_template('galaxy.xml')
    version = get_distribution('majiq_spel').version

    if not os.path.isdir(constants.GALAXY_DIR):
        raise Exception('GALAXY_DIR is not found.')

    tools_dir = os.path.join(constants.GALAXY_DIR, 'tools', constants.TOOL_DIR)
    if not os.path.isdir(tools_dir):
        os.makedirs(tools_dir)

    xml_file = os.path.join(tools_dir, constants.XML_FILE)

    advance_options = (
        '--min_length_exclude_limit',
        '--max_length',
        '--gcs_content_min',
        '--gcs_content_max',
        '--gc_content_difference_limit',
        '--primer_length_min',
        '--primer_length_max',
        '--tm_min',
        '--tm_max',
        '--gcs_3prime_min',
        '--gcs_3prime_max',
    )

    with open(xml_file, 'w') as xml:
        xml.write(template.render(
            version=version,
            melting_temp_formulas=MeltingTempFormulas().xml,
            parser=get_parser()._option_string_actions,
            advance_options=advance_options
        ))


def add_to_tool_conf():
    """
    Add this tool to the galaxy tool conf.
    :return: 
    """
    config_dir = os.path.join(constants.GALAXY_DIR, 'config')
    config_file = os.path.join(config_dir, 'tool_conf.xml')

    et = ElementTree.parse(config_file)
    toolbox = et.getroot()

    spel_id = 'majiq_spel'
    spel_name = 'MAJIQ-SPEL'

    if not sum(section.attrib['id'] == spel_id for section in toolbox):
        majiq_spel_section = ElementTree.SubElement(toolbox, 'section')
        majiq_spel_section.attrib['name'] = spel_name
        majiq_spel_section.attrib['id'] = spel_id
        majiq_spel_tool = ElementTree.SubElement(majiq_spel_section, 'tool')
        majiq_spel_tool.attrib['file'] = os.path.join(constants.TOOL_DIR, constants.XML_FILE)
        et.write(config_file)


def install_static():
    """
    Install static files to run this tool on Galaxy.
    :return: 
    """
    static_dir = os.path.join(constants.GALAXY_DIR, 'static', 'majiq_spel')

    try:
        shutil.rmtree(static_dir)
    except OSError:
        pass

    shutil.copytree(os.path.join('static', 'majiq_spel'), static_dir)


if __name__ == "__main__":

    # To run on galaxy, these are the required local constants.
    required_consants = ('GALAXY_DIR', 'GALAXY_URL_DIR', 'GALAXY_HOST')
    unset_constants = []
    for constant in required_consants:
        if constant not in constants.__dict__:
            unset_constants.append(constant)

    if unset_constants:
        print('{0} has not been set.'.format(', '.join(unset_constants)))
        exit(1)

    install_tool_xml()
    add_to_tool_conf()
    install_static()
