import os
from collections import namedtuple

HEX_COLORS = {'pink': '#ecafce', 'brown': '#7e5724', 'turquoise': '#80bcb2',
              'goldenrod': '#e4d465', 'purple': '#aaa6c5', 'blue': '#769fbb',
              'grey': '#c3c3c3', 'yellow': '#eded9a', 'green': '#a0c363',
              'light green': '#b5d7ae', 'violet': '#a776a8', 'orange': '#e0a25c',
              'red': '#e07468'}

COLOR_ORDER = ['red', 'blue', 'green', 'purple', 'orange', 'goldenrod', 'pink', 'turquoise', 'violet',
               'light green', 'yellow']

ANNOTATION_FILE = 'ucsc/annotation.gz'
MAJIQ_SPEL_DIR = os.path.dirname(os.path.realpath(__file__))

TOOL_DIR = 'majiq_spel'
XML_FILE = 'majiq_spel.xml'

DEBUG = False

# from the ucsc documentation:  0 - hide, 1 - dense, 2 - full, 3 - pack, and 4 - squish
VISIBILITY = namedtuple('VISIBILITY', 'hide dense full pack squish')(0, 1, 2, 3, 4)

LSV_TEXT_VERSION = 5

try:
    from local_constants import *
except ImportError:
    pass
