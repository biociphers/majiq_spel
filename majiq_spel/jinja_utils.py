import os

import jinja2

from majiq_spel import constants


def make_url(filename, subfolder):
    """
    Create url for a file within a folder.
    :param filename: file name
    :param subfolder: folder name
    :return: 
    """
    path = os.path.join('static', 'majiq_spel', subfolder, filename)
    if not constants.DEBUG:
        path = os.path.join(constants.GALAXY_HOST, constants.GALAXY_URL_DIR, path)
    return path


def css_url(css_filename):
    """
    Make URL for css file.
    :param css_filename: 
    :return: 
    """

    return make_url(css_filename, 'css')


def js_url(js_filename):
    """
    Make URL for js file.
    :param js_filename: 
    :return: 
    """

    return make_url(js_filename, 'js')


def img_url(img_filename):
    """
    Make URL for image file.
    :param img_filename: 
    :return: 
    """
    return make_url(img_filename, 'img')


def json_url(json_filename):
    """
    Make URL for json file. 
    :param json_filename: 
    :return: 
    """
    json_path = os.path.join('json', json_filename)
    if constants.DEBUG:
        json_path = os.path.join('static', json_path)
    return json_path


def label(label):
    """
    When creating the tool XML file, this helps parse the form labels. 
    :param label: label string value
    :return: 
    """
    label = label.replace('-', '').replace('_', ' ').replace('3prime', '3\'')
    label = ' '.join(i.capitalize() for i in label.split(' '))
    return label.replace('Lsv', 'LSV').replace('Gc', 'GC')


def type_to_string(type_value):
    """
    Translate the argparse type to html input type value. 
    :param type_value: type value within argparse
    :return: 
    """
    if type_value is int:
        return 'integer'
    if type_value is str or type_value is None:
        return 'text'
    if type_value is float:
        return 'float'
    raise Exception('Unknown type: {0}'.format(type_value))


def get_env():
    """
    Get jinja environment.
    :return: 
    """
    env = jinja2.Environment(
        loader=jinja2.PackageLoader('majiq_spel', 'templates'),
    )
    env.globals.update({
        'json_url': json_url,
        'js_url': js_url,
        'css_url': css_url,
        'img_url': img_url,
        'label': label,
        'type_to_string': type_to_string
    })
    return env
