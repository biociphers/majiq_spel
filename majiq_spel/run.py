import argparse
import os
import uuid

from majiq_spel import ucsc, constants
from majiq_spel import utils
from majiq_spel.constants import ANNOTATION_FILE, VISIBILITY
from majiq_spel.isoforms import LSVPaths, get_isoforms, get_violin_bins, color_splice_graph
from majiq_spel.jinja_utils import get_env
from majiq_spel.lsv import get_lsv_dict
from majiq_spel.melting_temp_formulas import MeltingTempFormulas
from majiq_spel.primer_design import get_all_valid_primer_pairs
from majiq_spel.splice_graph import visualize_sg, intron_retention
from majiq_spel.ucsc import get_dna, AnnotationFile, NoUcscDnaData, ScrewyStartAndEndExons


def primers(args):
    """
    Create primer json files and ucsc annotation file. 
    :param args: arguments
    :return: error 
    """
    lsv_dict = utils.read_json(args, 'lsv.json')
    ucsc_error = ''

    color_splice_graph(lsv_dict)

    if utils.is_minus_strand(lsv_dict):
        utils.reverse_lsv(lsv_dict)

    exons_dna = []
    try:
        exons_dna = get_dna(lsv_dict)
    except NoUcscDnaData:
        ucsc_error = 'Unable to retrieve nucleotide information from UCSC. Verify your genome is available with ' \
                     '<a href="https://genome.ucsc.edu/cgi-bin/hgGateway"> UCSC\'s Genome Browser</a>.'
    except ScrewyStartAndEndExons:
        ucsc_error = 'Unable to retrieve nucleotide information from UCSC. Primers complementary to the outermost ' \
                     'flanking exons of this LSV are not suitable for proper quantification via RT-PCR.'

    primer_pairs = get_all_valid_primer_pairs(exons_dna, args)
    ucsc.add_start_end_location(primer_pairs, lsv_dict)

    forward = {}
    reverse = {}

    for primer in primer_pairs:
        forward[primer['forward']['primer']] = primer['forward'].copy()
        reverse[primer['reverse']['primer']] = primer['reverse'].copy()
        del forward[primer['forward']['primer']]['primer']
        del reverse[primer['reverse']['primer']]['primer']

    for index, (_, fwd) in enumerate(forward.items()):
        fwd['index'] = 'f{0}'.format(index)

    for index, (_, rev) in enumerate(reverse.items()):
        rev['index'] = 'r{0}'.format(index)

    primers_dict = dict(
        [('{0}:{1}'.format(primer_pair['forward']['primer'], primer_pair['reverse']['primer']), {}) for primer_pair in
         primer_pairs]
    )

    utils.write_json(forward, args, 'forward.json')
    utils.write_json(reverse, args, 'reverse.json')
    utils.write_json(primers_dict, args, 'primers.json')

    return ucsc_error


def annotation_file(args):
    lsv_dict = utils.read_json(args, 'lsv.json')
    forward = utils.read_json(args, 'forward.json')
    reverse = utils.read_json(args, 'reverse.json')

    af = AnnotationFile(lsv_dict, os.path.join(args.output_dir, ANNOTATION_FILE))
    af.add_track(name='Exons', description='Exons', visibility=VISIBILITY.dense)
    af.add_exons()
    af.add_track(name='Intron Retention', description='Intron Retention', visibility=VISIBILITY.dense)
    af.add_intron_retention()
    af.add_track(name='Junctions', description='Junctions', visibility=VISIBILITY.squish)
    af.add_junctions()
    af.add_track(name='Forward Primers', description='Forward Primers', visibility=VISIBILITY.pack)
    af.add_forward_primers(forward)
    af.add_track(name='Reverse Primers', description='Reverse Primers', visibility=VISIBILITY.pack)
    af.add_reverse_primers(reverse)
    af.write_output()


def isoforms(args):
    """
    Create isoforms json files. 
    :param args: arguments 
    :return: 
    """
    lsv_dict = utils.read_json(args, 'lsv.json')

    if utils.should_reverse_lsv_dict(lsv_dict):
        utils.reverse_lsv(lsv_dict)
        paths = LSVPaths(lsv_dict).reverse()
    else:
        paths = LSVPaths(lsv_dict).forward()

    isoforms = get_isoforms(paths)

    violin_bins = get_violin_bins(lsv_dict)

    isoform_table = [{'violins': violins, 'isoforms': [], 'color_index': color_index} for color_index, violins in
                     enumerate(violin_bins)]

    for isoform in isoforms:
        color_index = isoform['color_index']
        exons = isoform['exons']
        isoform_table[color_index]['isoforms'].append({'exons': exons, 'id': uuid.uuid4().hex})

    for v in isoform_table:
        v['color'] = utils.get_color(v['color_index'])
        del v['color_index']

    if utils.should_reverse_isoform(lsv_dict):
        utils.reverse_isoform(isoform_table)

    utils.write_json(isoform_table, args, 'isoforms.json')


def splice_graphs(args):
    """
    Create splice graphs json files. 
    :param args: arguments
    :return: 
    """
    lsv_dict = utils.read_json(args, 'lsv.json')

    color_splice_graph(lsv_dict)

    splice_graphs = visualize_sg(lsv_dict)

    intron_retention(splice_graphs)

    backwards = utils.is_target(lsv_dict)
    if backwards:
        utils.reverse_sg(splice_graphs)

    for splice_graph in splice_graphs:
        splice_graph['backwards'] = backwards

        utils.write_json(splice_graphs, args, 'splicegraphs.json')


def isoform_lengths(args):
    """
    Caculate isoform length as add them to primers json files.
    :param args: arguments 
    :return: 
    """
    primers_dict = utils.read_json(args, 'primers.json')
    isoforms_list = utils.read_json(args, 'isoforms.json')
    lsv_dict = utils.read_json(args, 'lsv.json')
    forward_dict = utils.read_json(args, 'forward.json')
    reverse_dict = utils.read_json(args, 'reverse.json')

    for isoform_dict in isoforms_list:
        for isoform in isoform_dict['isoforms']:
            isoform['length'] = sum(exon['end'] - exon['start'] for exon in isoform['exons'])

    utils.write_json(isoforms_list, args, 'isoforms.json')

    for pair in primers_dict:

        primer_pair = pair.split(':')

        forward = forward_dict[primer_pair[0]]
        reverse = reverse_dict[primer_pair[1]]

        if utils.is_minus_strand(lsv_dict):

            for isoform_dict in isoforms_list:
                for isoform in isoform_dict['isoforms']:
                    exons = isoform['exons']

                    if (exons[0]['start'] <= forward['end'] <= exons[0]['end']) and (
                                    exons[-1]['start'] <= reverse['start'] <= exons[-1]['end']):
                        middle_exons = sum([exon['end'] - exon['start'] for exon in exons[1:-1]])
                        left_exon = forward['end'] - exons[0]['start']
                        right_exon = exons[-1]['end'] - reverse['start']
                        primers_dict[pair][isoform['id']] = middle_exons + left_exon + right_exon + (len(exons) - 1)

        else:

            for isoform_dict in isoforms_list:
                for isoform in isoform_dict['isoforms']:
                    exons = isoform['exons']

                    if (exons[0]['start'] <= forward['start'] <= exons[0]['end']) and (
                                    exons[-1]['start'] <= reverse['end'] <= exons[-1]['end']):
                        middle_exons = sum([exon['end'] - exon['start'] for exon in exons[1:-1]])
                        left_exon = exons[0]['end'] - forward['start']
                        right_exon = reverse['end'] - exons[-1]['start']
                        primers_dict[pair][isoform['id']] = middle_exons + left_exon + right_exon + (len(exons) - 1)

    utils.write_json(primers_dict, args, 'primers.json')


def get_parser():
    """
    MAJIQ-SPEL argument parser.
    :return: parser
    """

    parser = argparse.ArgumentParser()
    parser.add_argument('--output_dir',
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'static'))
    parser.add_argument('--output_html',
                        default=os.path.join(os.path.dirname(os.path.realpath(__file__)), 'index.html'))
    parser.add_argument('--lsv',
                        help='Local splicing variation. MAJIQ formulation of alternative splicing where splice '
                             'junctions emanate from the reference exon (source LSV) or converge into the reference '
                             'exon (target LSV).')
    # Advance options...
    parser.add_argument('--min_length_exclude_limit', default=90, type=int,
                        help='The minimum RT-PCR product nucleotide length allowed for the exclusion product for a '
                             'forward/reverse primer pair to be considered valid.')

    parser.add_argument('--max_length', default=1000, type=int,
                        help='The maximum nucleotide length of an RT-PCR product. Products over this length will '
                             'be marked with a flag in the isoform table.')

    parser.add_argument('--gcs_content_min', default=0.5, type=float,
                        help='The minimum GC content (number of GCs / primer length) allowed for a primer.')

    parser.add_argument('--gcs_content_max', default=0.6, type=float,
                        help='The maximum GC content (number of GCs / primer length) allowed for a primer.')

    parser.add_argument('--gc_content_difference_limit', default=10, type=int,
                        help='The maximum difference in GC content (number of GCs / primer length) allowed between'
                             ' possible forward and reverse primers.')

    parser.add_argument('--primer_length_min', default=22, type=int,
                        help='The minimum primer nucleotide length allowed.')

    parser.add_argument('--primer_length_max', default=30, type=int,
                        help='The maximum primer nucleotide length allowed.')

    parser.add_argument('--melting_temp_formula', default=MeltingTempFormulas().default,
                        help='Formula used to estimate primer melting temperatures.  '
                             'Marmur: Tm = 4 * (G+C) + 2 * (A+T); '
                             'Wallace: Tm = 64.9 + 41 * (G+C-16.4) / (A+T+G+C).')

    parser.add_argument('--tm_min', default=76, type=int,
                        help='Minimum melting temperature allowed for primer based on selected formula.')

    parser.add_argument('--tm_max', default=92, type=int,
                        help='Maximum melting temperature allowed for primer based on selected formula.')

    parser.add_argument('--gcs_3prime_min', default=2, type=int,
                        help='Minimum number of GCs allowed at 3\' end of primer to allow for strong pairing at the '
                             '3\' end of the primer to allow for elongation.')
    parser.add_argument('--gcs_3prime_max', default=4, type=int,
                        help='Maximum number of GCs allowed at 3\' end of primer to avoid strong GC clamp formation.')
    parser.add_argument('--debug', action='store_true', help='Run in debug mode.')
    parser.add_argument('--httpd', action='store_true', help='If ran in debug mode, start a http daemon.')

    return parser


def main():
    args = get_parser().parse_args()

    if args.debug:
        constants.DEBUG = True

    raw_data_dict = utils.validate(args.lsv)

    lsv_dict = get_lsv_dict(raw_data_dict)

    utils.write_json(lsv_dict, args, 'lsv.json')

    ucsc_error = primers(args)

    annotation_file(args)

    isoforms(args)

    splice_graphs(args)

    isoform_lengths(args)

    if utils.is_target(lsv_dict):
        source_target = 'Target'
    else:
        source_target = 'Source'

    wp_target = 'genome'

    template = get_env().get_template('index.html')
    with open(args.output_html, 'w') as html:
        lsv_dict = utils.read_json(args, 'lsv.json')
        html.write(template.render(
            strand=lsv_dict['strand'],
            lsv_id=lsv_dict['lsv_id'],
            genome=lsv_dict['genome'],
            gene_name=lsv_dict['name'],
            chromosome=lsv_dict['chromosome'],
            ucsc_annotation_file=constants.ANNOTATION_FILE,
            source_target=source_target,
            ucsc_error=ucsc_error,
            min_length_exclude_limit=args.min_length_exclude_limit,
            max_length=args.max_length,
            wp_target=wp_target,
            sample_names=lsv_dict['sample_names']
        ))

    if args.debug and args.httpd:

        httpd = None
        try:
            import http.server
            import socketserver
            PORT = 8000
            os.chdir(os.path.dirname(os.path.realpath(__file__)))
            socketserver.TCPServer.allow_reuse_address = True
            Handler = http.server.SimpleHTTPRequestHandler
            httpd = socketserver.TCPServer(("", PORT), Handler)
            print("Serving at http://localhost:{0}".format(PORT))
            httpd.serve_forever()
        except KeyboardInterrupt:
            httpd.shutdown()

            # p = psutil.Process(os.getpid())
            # print p.memory_info()
            # print p.cpu_times()


if __name__ == "__main__":
    main()
