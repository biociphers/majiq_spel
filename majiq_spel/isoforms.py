from majiq_spel import utils


def overlapping_triples(itr):
    """
    parses iterator to overlapping triples.
    [1,2,3,4,5,6,7...n] -> (1,2,3), (3,4,5), (5,6,7), ..., (n-2,n-1,n)
    :param itr: iterator
    :return: generator
    """

    return (itr[t - 3: t] for t in range(3, len(itr) + 1, 2))


def color_splice_graph(lsv_dict):
    """
    Add colors to the splice graph junctions and exons.
    :param lsv_dict: 
    :return: 
    """
    if utils.should_reverse_lsv_dict(lsv_dict):
        utils.reverse_lsv(lsv_dict)

    color_junctions(lsv_dict)
    color_exons(lsv_dict)

    if utils.should_reverse_lsv_dict(lsv_dict):
        utils.reverse_lsv(lsv_dict)


def color_junctions(lsv_dict):
    """
    Add 'color' field to each junction.
    :param lsv_dict: lsv data
    :return:
    """
    for splice_graph in lsv_dict['splice_graphs']:
        exons = splice_graph['exons']
        junctions = splice_graph['junctions']

        junc_indices = exons[0]['a5']

        if utils.is_target(lsv_dict):
            junc_indices = list(reversed(junc_indices))

        color_index = 0

        for junc_index in junc_indices:
            # if not junctions[junc_index]['intron_retention']:
            junctions[junc_index]['color_index'] = color_index
            color_index += 1

        # juctions in intron retention should be colored last
        # for junc_index in junc_indices:
        #     if junctions[junc_index]['intron_retention']:
        #         junctions[junc_index]['color_index'] = color_index
        #         color_index += 1


def color_exons(lsv_dict):
    """
    Add 'color' field to each exon.
    :param lsv_dict: lsv data
    :return:
    """
    for splice_graph in lsv_dict['splice_graphs']:
        exons = splice_graph['exons']
        junctions = splice_graph['junctions']

        exons[0]['color_index'] = -1
        for exon in exons[1:]:
            if exon['a3']:
                junc_index = exon['a3'][0]
                exon['color_index'] = junctions[junc_index].get('color_index', None)


class LSVPaths(object):
    def __init__(self, lsv_dict):
        """
        Finds all the paths through the lsv.
        :param lsv_dict: lsv data
        """
        color_junctions(lsv_dict)
        color_exons(lsv_dict)

        self.isoforms = []
        self._reverse = False

        # we can use either splice graph to find the possible paths
        splice_graph = lsv_dict['splice_graphs'][0]
        self.exons = splice_graph['exons']
        self.junctions = splice_graph['junctions']

    def forward(self):
        """
        Find paths going forward through the splice graph.
        :return: list of isoforms
        """
        self._dfs(0)
        return self.isoforms

    def reverse(self):
        """
        Find paths going through the splice graph when it has been reversed. (see utils/reverse_lsv)
        return: list of isoforms
        """
        self._reverse = True
        self._dfs(0)
        return self.isoforms

    def _after_splice_sites(self, incoming_junc, exon):
        """
        Find splice sites after this incoming junction for this exon.
        :param incoming_junc: junction incoming to exon
        :param exon: our exon
        :return: 
        """
        junctions = self.junctions
        junc_ends_dict = {}
        after_splice_sites = []

        if not incoming_junc:
            return []

        for j in exon['a3']:
            color_index = junctions[j].get('color_index', None)
            end = junctions[j]['end']

            try:
                # we don't want two colors in a row
                if junc_ends_dict[end][-1] != color_index:
                    junc_ends_dict[end].append(color_index)
            except KeyError:
                junc_ends_dict[end] = [color_index]

        for end, color_indices in sorted(list(junc_ends_dict.items()), reverse=self._reverse):
            if len(color_indices) > 1:
                color_indices = [c for c in color_indices if c]

            if self._reverse:
                if end < incoming_junc['end']:
                    after_splice_sites += color_indices
            else:
                if end > incoming_junc['end']:
                    after_splice_sites += color_indices

        return after_splice_sites

    def _prior_splice_sites(self, incoming_junc, outgoing_junc, exon):
        """
        Find the number of splice sites prior to the incoming junction.  
        :param incoming_junc: 
        :param outgoing_junc: 
        :param exon: 
        :return: 
        """
        junctions = self.junctions

        prior_splice_sites = set()
        for j in exon['a5']:

            if self._reverse:
                is_prior_start_site = junctions[j]['start'] > outgoing_junc['start']
            else:
                is_prior_start_site = junctions[j]['start'] < outgoing_junc['start']

            # filter out junctions that start before the incoming junction ends
            if self._reverse:
                wierd_junction = 'end' in incoming_junc and junctions[j]['start'] > incoming_junc['end']
            else:
                wierd_junction = 'end' in incoming_junc and junctions[j]['start'] < incoming_junc['end']

            if is_prior_start_site and not wierd_junction:
                prior_splice_sites.add(junctions[j]['start'])

        return len(prior_splice_sites)

    def color_incoming_junc(self, incoming_junc, exon):
        """
        Color incoming junction when junction doesn't have a color and may end with another junction that does have a 
        color.
        :param incoming_junc: 
        :param exon: 
        :return: 
        """
        # search through other incoming junctions for a color
        if incoming_junc and 'color_index' not in incoming_junc:
            for j in exon['a3']:
                if 'color_index' in self.junctions[j] and self.junctions[j]['end'] == incoming_junc['end']:
                    incoming_junc['color_index'] = self.junctions[j]['color_index']

    def end_of_path(self, starting_exon_index, paths):
        """
        Properly record end of a path. 
        :param starting_exon_index: 
        :param paths: 
        :return: 
        """
        exons = self.exons
        incoming_junc = paths[-1]
        exon = exons[starting_exon_index].copy()
        exon['prior_splice_sites'] = 0
        exon['after_splice_sites'] = self._after_splice_sites(incoming_junc, exon)
        exon['exon_index'] = starting_exon_index

        self.color_incoming_junc(incoming_junc, exon)

        try:
            exon['color_index'] = incoming_junc['color_index']
        except KeyError:
            pass

        self.isoforms.append(paths + [exon, {}])

    def _dfs(self, starting_exon_index, paths=None, history=None):
        """
        Using recursive DFS, Find all paths through LSV. 
        
        :param starting_exon_index:  Staring exon
        :param paths: All found paths. 
        :param history: list of junction indices. 
        :return: 
        """
        exons = self.exons

        if paths is None:
            paths = [{}]

        incoming_junc = paths[-1]

        if history is None:
            history = []

        # filter out unneeded junctions
        a5_juncs = []
        for junc_index in exons[starting_exon_index]['a5']:
            outgoing_junc = self.junctions[junc_index]
            if 'end' in incoming_junc:
                if self._reverse:
                    if incoming_junc['end'] < outgoing_junc['start']:
                        continue
                else:
                    if incoming_junc['end'] > outgoing_junc['start']:
                        continue

            a5_juncs.append(junc_index)

        if not a5_juncs:
            self.end_of_path(starting_exon_index, paths)

        for junc_index in a5_juncs:
            outgoing_junc = self.junctions[junc_index]

            if 'end' in incoming_junc:
                if self._reverse:
                    if incoming_junc['end'] < outgoing_junc['start']:
                        continue
                else:
                    if incoming_junc['end'] > outgoing_junc['start']:
                        continue

            for exon_index, exon in enumerate(exons):
                if junc_index in exon['a3']:

                    if junc_index in history:
                        continue

                    exon = exons[starting_exon_index].copy()
                    exon['prior_splice_sites'] = self._prior_splice_sites(incoming_junc, outgoing_junc, exon)
                    exon['after_splice_sites'] = self._after_splice_sites(incoming_junc, exon)
                    exon['exon_index'] = starting_exon_index

                    self.color_incoming_junc(incoming_junc, exon)

                    try:
                        exon['color_index'] = incoming_junc['color_index']
                    except KeyError:
                        pass

                    self._dfs(exon_index, paths + [exon, outgoing_junc], history + [junc_index])


def get_isoform(xxx_todo_changeme):
    """
    Helper function to parse isoform data. 
    :return: 
    """
    (incoming_junc, exon, outgoing_junc) = xxx_todo_changeme
    start = incoming_junc.get('end', exon['start'])
    end = outgoing_junc.get('start', exon['end'])

    return {
        'exon_index': exon['exon_index'],
        # 'intron_retention': exon['intron_retention'],
        'start': min(start, end),
        'end': max(start, end)
    }


def get_isoforms(paths):
    """
    Create isoforms from paths.
    :param paths: paths value from LSVPaths class.
    :return: list of isoforms
    """
    isoform_list = []

    # By using the prior and after splice sites, we can determine how to segment the exon and how to color it.
    for path in paths:

        isoform_exons = []

        for isoform_index, overlapping_tripple in enumerate(overlapping_triples(path)):

            # length for each exon is calculated here.  Length for each section is calculated in the js.  This is
            # because we need to adjust the length depending on which primer is selected.

            # exon is always the second element in the tripple.
            exon = overlapping_tripple[1]

            sections = []

            isoform_exon = get_isoform(overlapping_tripple)

            try:
                color = utils.get_color(exon['color_index'])
            except KeyError:
                color = None

            sections.append({
                'color': color
            })

            for color_index in exon['after_splice_sites']:

                try:
                    color = utils.get_color(color_index)
                except KeyError:
                    color = None

                sections.append({
                    'color': color
                })

            for _ in range(exon['prior_splice_sites']):

                if color:
                    color = utils.darken_color(color)

                sections.append({
                    'color': color
                })

            isoform_exon['sections'] = sections
            isoform_exons.append(isoform_exon)

        isoform_list.append({
            'exons': isoform_exons,
            'color_index': path[3]['color_index']
        })

    return isoform_list


def get_violin_bins(lsv_dict):
    """
    Get violin bins depending if this data is deltapsi or psi.
    :param lsv_dict: dictionary of lsvs
    :return: voila bins
    """
    bins = lsv_dict['bins']
    if len(bins) == 2:
        return list(zip(bins[0], bins[1]))
    if len(bins) == 1:
        return ([l] for l in bins[0])
