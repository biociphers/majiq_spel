Create environment and install package:
```
#!bash
# Virtual environment must be python 2.7.  Please adjust the Python executable for your system. 
virtualenv -p python2.7 env
source env/bin/activate
git clone https://bitbucket.org/biociphers/majiq_spel.git
python majiq_spel/setup.py build
python majiq_spel/setup.py install
```

View usage statement:
```
#!bash
majiq_spel --help
```

MAJIQ-SPEL is intented to be a Galaxy Tool, but to generate index.html file for debugging purposes the following 
command could be used.  The html file will still have to be viewed using a http daemon for the XMLHtmlRequests to 
work properly. 
```
#!bash
majiq_spel --debug --lsv "<very long lsv string>"
```

To use the simple Python HTTP daemon you could run the following command.
```
#!bash
majiq_spel --debug --httpd --lsv "<very long lsv string>"
```

MAJIQ-SPEL documentation can be found at: http://majiq.biociphers.org/majiq-spel/docs